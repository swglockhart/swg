#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin

DBSERVICE=
DBUSERNAME=
DBPASSWORD=
HOSTIP=
CLUSTERNAME=
NODEID=
DSRC_DIR=
DATA_DIR=

if [ ! -d $basedir/build ]
then
	mkdir $basedir/build
fi
	
read -p "Do you want to build the scripts now? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH
	$basedir/utils/mocha/prepare_all_scripts_multi.sh $basedir/dsrc/sku.0/sys.server/compiled/game/script
fi

read -p "Do you want to build the java files? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH

	$basedir/utils/build_java_multi.sh
	PATH=$oldPATH
fi
 
read -p "Do you want to Generate IFFs? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH

	$basedir/utils/build_miff.sh

	PATH=$oldPATH
fi	
  
read -p "Do you want to build Template (tdf) files? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH

	$basedir/utils/build_tdf.sh

	PATH=$oldPATH
fi

read -p "Do you want to Generate the Datatables (tab) Files? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH

	$basedir/utils/build_tab_multi.sh

	PATH=$oldPATH	  
fi

read -p "Do you want to Generate Object Templates (tpf) Files? (y/n) " response
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]]; then
	#prepare environment to run data file builders
	oldPATH=$PATH
	PATH=$basedir/build/bin:$PATH

	$basedir/utils/build_tpf_multi.sh

	PATH=$oldPATH 
fi
	
read -p "Do you want to import SWG database into Oracle? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then
	cd $basedir/opt/database/build/linux

	if [[ -z "$DBSERVICE" ]]; then
		echo "Enter the DSN for the database connection "
		read DBSERVICE
	fi

	if [[ -z "$DBUSERNAME" ]]; then
		echo "Enter the database username "
		read DBUSERNAME
	fi

	if [[ -z "$DBPASSWORD" ]]; then
		echo "Enter the database password "
		read DBPASSWORD
	fi

	./database_update.pl --username=$DBUSERNAME --password=$DBPASSWORD --service=$DBSERVICE --goldusername=$DBUSERNAME --loginusername=$DBUSERNAME --createnewcluster --packages

	echo "Loading object_template_crc_string_table into your oracle template list"
	perl $basedir/opt/database/templates/processTemplateList.pl < $basedir/swgdb/object_template_crc_string_table.tab > $basedir/build/templates.sql
	sqlplus ${DBUSERNAME}/${DBPASSWORD}@${DBSERVICE} @$basedir/build/templates.sql > $basedir/build/templates.out
	cd $basedir
fi
echo "Creating the Server Preload Files! (Enable with [Gameserver] enablepreload= 1=on 0=off"
find ./data/sku.0/sys.server/compiled/game/datatables/ -name *.iff > $basedir/bin/win32/datatables.plf
find ./data/sku.0/sys.server/compiled/game/object/ -name *.iff > $basedir/bin/win32/objectTemplates.plf

# cp -v $basedir/data/sku.0/sys.server/compiled/game/datatables/datatables.plf $basedir/bin/win32/datatables.plf 
# cp -v $basedir/data/sku.0/sys.server/compiled/game/object/objectTemplates.plf $basedir/bin/win32/objectTemplates.plf
echo "Congratulations your server build is complete!"