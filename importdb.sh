#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin

DBSERVICE=
DBUSERNAME=
DBPASSWORD=
HOSTIP=
CLUSTERNAME=
NODEID=
DSRC_DIR=
DATA_DIR=


read -p "Do you want to import swg database into Oracle? (y/n) " response
response=${response,,}
if [[ $response =~ ^(yes|y| ) ]]; then
	cd $basedir/opt/database/build/linux

	if [[ -z "$DBSERVICE" ]]; then
		echo "Enter the DSN for the database connection "
		read DBSERVICE
	fi

	if [[ -z "$DBUSERNAME" ]]; then
		echo "Enter the database username "
		read DBUSERNAME
	fi

	if [[ -z "$DBPASSWORD" ]]; then
		echo "Enter the database password "
		read DBPASSWORD
	fi

	./database_update.pl --username=$DBUSERNAME --password=$DBPASSWORD --service=$DBSERVICE --goldusername=$DBUSERNAME --loginusername=$DBUSERNAME --createnewcluster --packages

	echo "Loading template list"
	perl $basedir/opt/database/templates/processTemplateList.pl < $basedir/opt/object_template_crc_string_table.tab > $basedir/build/templates.sql
	sqlplus ${DBUSERNAME}/${DBPASSWORD}@${DBSERVICE} @$basedir/build/templates.sql > $basedir/build/templates.out
	cd $basedir
fi
echo "Creating Preload Files!"
find ./data/sku.0/sys.server/compiled/game/datatables/ -name *.iff > $basedir/bin/win32/datatables.plf
find ./data/sku.0/sys.server/compiled/game/object/ -name *.iff > $basedir/bin/win32/objectTemplates.plf

# cp -v $basedir/data/sku.0/sys.server/compiled/game/datatables/datatables.plf $basedir/bin/win32/datatables.plf 
# cp -v $basedir/data/sku.0/sys.server/compiled/game/object/objectTemplates.plf $basedir/bin/win32/objectTemplates.plf
echo "Build complete!"
