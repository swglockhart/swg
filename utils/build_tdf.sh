#!/bin/bash

basedir=$PWD

find $basedir/dsrc -name '*.tdf' | while read filename; do
	echo $filename
	$basedir/tools/TemplateDefinitionCompiler -compile $filename
done
